const express = require('express');
const app = express();
const http = require('http').Server(app);
const fs = require('fs');
const path = require('path');
const dirTree = require('directory-tree');
const opn = require('opn');
const argv = require('minimist')(process.argv.slice(2));

/** @type string Execution environment */
const environment = (typeof argv.env !== typeof undefined && argv.env === 'production') ? 'prod' : 'dev';

/** @type string Public web root */
const publicRoot = path.join(__dirname, 'public');

/** @type string Absolute path for image gallery */
const galleryPath = path.join(__dirname, 'public', 'image-gallery');

/** @type string Absolute path for image gallery index */
const galleryIndexFile = path.join(galleryPath, 'file-index.json');

// Create files index
if (!fs.existsSync(galleryIndexFile) || environment === 'prod') {
  const fileIndex = dirTree(galleryPath, { exclude: /\.(json)$/, normalizePath: true });

  // Add file creation date
  if (environment !== 'prod') {
    fileIndex.index_generated_at = new Date();
  }
  
  /** @type string Image gallery index as JSON string */
  const jsonData = JSON.stringify(fileIndex, null, 2).replace((new RegExp(publicRoot.replace(/\\/g, '/'), 'g')), '');
  fs.writeFileSync(galleryIndexFile, jsonData);
}

// Serve static files
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(publicRoot));

// Server listen localhost:8000
http.listen(8000, () => {
  console.log('Example app listening on port 8000!');
  if (environment === 'prod') opn('http://localhost:8000/index.html');
});