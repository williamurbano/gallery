function addChilds(list, item) {
  let link = item.type !== 'directory' ? 'file-link' : '';
  let icon = item.type !== 'directory' ? 'fa-image' : 'fa-folder';
  let file = item.type !== 'directory' ? item.path : '';

  let listItem = $($.parseHTML(`<li>
    <a class="${link} p-2 text-white d-block" href="javascript:void(0);" data-file="${file}" data-name="${item.name}">
      <i class="fas ${icon}"></i>&nbsp;&nbsp;${item.name}
    </a>

    <ul class="list-unstyled"></ul>
  </li>`));

  $(list).append(listItem);

  if (typeof item.children !== typeof undefined && item.children.length > 0) {
    let _list = listItem.find('ul');
    item.children.map((subitem) => addChilds(_list, subitem));
  }
}

$(document).ready(function () {
  $.ajax({
    url: '/image-gallery/file-index.json',
    type: 'get',
    dataType: 'json'
  }).done(function (data) {
    let list = $('.file-index').empty();
    let items = data.children[0];

    addChilds(list, items);

    $('.file-link').first().click();
  });

  // handle image change
  $(document).on('keyup', function (e) {
    let links = $('.file-link'),
      activateIndex = null;

    if (e.which == 39) { // go next
      links.each(function (index) {
        if ($(this).is('.active')) {
          activateIndex = index + 1;
        }
      }).promise().done(function () {
        $(this).each(function (index) {
          if (index === activateIndex) {
            $(this).click();
          }
        });
      });
    } else if (e.which == 37) { // go previous
      links.each(function (index) {
        if ($(this).is('.active')) {
          activateIndex = index - 1;
        }
      }).promise().done(function () {
        $(this).each(function (index) {
          if (index === activateIndex) {
            $(this).click();
          }
        });
      });
    }
  });
    
  // Remove class for empty image
  $('#image-item').on('load', function () {
    $(this).removeClass('empty');
  });

  // Change selected image
  $(document).on('click', '.file-link', function () {
    let file = $(this).data('file'),
      name = file.replace('/image-gallery/', '').replace(/\//g, ' > ');

    // Toggle active link
    $('.file-link.active').removeClass('active');
    $(this).addClass('active');

    // Change file
    $('#image-item').attr('src', '')
      .addClass('empty');

    $('#file-name-selected').text('');
    
    setTimeout(function () {
      $('#image-item').attr('src', file);
      $('#file-name-selected').text(name);
    }, 500);

    $('.close-sidenav').click();
  });

  $('.open-sidenav').click(function () {
    $('#sidenav').css('width', '400px');
    $('#main .overlay').css('opacity', 1);
  });

  $('.close-sidenav').click(function () {
    $('#sidenav').css('width', 0);
  });
});